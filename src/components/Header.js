import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Button, Input, Tooltip } from 'antd';
import { LoginOutlined } from '@ant-design/icons';
import { ControlOutlined } from '@ant-design/icons';
import RegularSeparator from './RegularSeparator';
import ModalFilter from './ModalFilter';
import { useHistory, Link } from 'react-router-dom';
/*import { useMediaQuery } from 'react-responsive'*/

const { Search } = Input;

const Header = (props) => {

   const { style, onSearch, onFilter, searcher, title } = props

   const [filterModalVisible, setFilterModalVisible] = useState(false)

   let history = useHistory();

   const logout = () => {
      localStorage.removeItem('logged')
      history.push('/')
   }

   return (
      <div style={style} className={'header-container'}>
         <div className={'header-content'}>
            <Link to={`/home`} className={'logo-button'}>
               <img alt={'logo'} height={"100%"} src={"https://i.ya-webdesign.com/images/news-transparent-white-1.png"}/>
            </Link>

            {
               searcher &&
               <div style={styles.searcherContainer}>
                  <Search
                     placeholder="Buscar"
                     onSearch={value => onSearch(value)}
                     size={"large"}
                  />

                  <RegularSeparator/>

                  <Tooltip title="Más filtros">
                     <Button size={"large"} icon={<ControlOutlined/>} onClick={() => setFilterModalVisible(true)}/>
                  </Tooltip>
               </div>
            }

            {
               title &&
                  <span className={'header-title'}>{title}</span>
            }

            <Button
               type="default"
               onClick={logout}
               ghost
               style={{ fontWeight: "bold" }}
               loading={false}
               shape="round"
               icon={
                  <LoginOutlined/>
               }
               size={40}
            >
               <span className={'sesion-button-txt'}>Cerrar sesión</span>
            </Button>

            <ModalFilter
               visible={filterModalVisible}
               onOk={(e) => onFilter(e)}
               onClose={() => setFilterModalVisible(false)}
            />
         </div>
      </div>
   )
}

const styles = {
   searcherContainer: {
      display: "flex",
      alignItems: "center",
      width: "60%"
   }
}

Header.propTypes = {
   style: PropTypes.any,
   onSearch: PropTypes.func,
   onFilter: PropTypes.func,
   searcher: PropTypes.bool,
   title:PropTypes.string
}

Header.defaultProps = {
   searcher: true
}

export default Header
