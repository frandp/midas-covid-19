import React, { useState } from 'react'
import { Modal, Button } from 'antd';
import PropTypes from 'prop-types'
import FilterForm from './forms/FilterForm';

const ModalFilter = (props) => {

   const { style, visible, onClose, onOk } = props

   const [dataFilterForm, setDataFilterForm] = useState({})

   const handleOk = () => {
      onOk(dataFilterForm) //Enviamos datos guardados en estado a través de la propiedad onOk
      onClose(false) //Cerramos modal a través de la propiedad onClose
   }

   return (
      <Modal
         style={style}
         visible={visible}
         title="Filtros"
         onOk={handleOk}
         onCancel={() => onClose(false)}
         footer={[
            <Button key="back" onClick={() => onClose(false)}>
               Cancelar
            </Button>,
            <Button key="submit" type="primary" onClick={handleOk}>
               Aceptar
            </Button>,
         ]}
      >
         <FilterForm onChangeValue={(e) => setDataFilterForm(e)}/>
      </Modal>
   )
}

ModalFilter.propTypes = {
   style: PropTypes.any,
   visible: PropTypes.bool,
   onClose: PropTypes.func,
   onOk: PropTypes.func
}

export default ModalFilter
