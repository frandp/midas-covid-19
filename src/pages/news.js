import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import Header from '../components/Header'
import LineSeparator from '../components/LineSeparator';
import axios from 'axios'
import { Spin } from 'antd';
import moment from 'moment'
import IdleTimerContainer from '../components/IdleTimerContainer';
import { useHistory } from 'react-router-dom'
import Footer from '../components/Footer'

const News = (props) => {

   const { style, match } = props

   const [news, setNews] = useState({})
   const [loading, setLoading] = useState(true)
   /*const [logged] = useState(JSON.parse(localStorage.getItem('logged')) || false)*/
   const logged = JSON.parse(localStorage.getItem('logged')) || false

   const urlId = match.params.id

   let history = useHistory()

   useEffect(() => {
      setLoading(true)
      if (logged) {
         // Mediante llamada axios obtenemos data de la Api
         axios.get('http://webhose.io/filterWebContent?token=b0c0b6ab-f0a1-46f7-98ee-8cc39c54e644&format=json&sort=crawled&q=coronavirus%20casos%20positivos%20%20language%3Aspanish%20thread.country%3AAR')
            .then(res => {
               let data = res.data.posts //Guardamos posts en constante data
               console.log('data', data)
               const actualNews = data.find(x => x.uuid === urlId)

               console.log('actual', actualNews)
               setNews(actualNews)
               setLoading(false)
            }).catch(err => {
            console.log(err)
         })
      } else {
         history.push("/")
      }
   }, [history, urlId, logged])

   return (
      loading ?
         <div className={"spin-container"}>
            <Spin size="large"/>
         </div>
         :
         <div style={style}>
            <IdleTimerContainer/> {/*Componente encargado de cerrar sesión por inactividad*/}
            <Header searcher={false} title={'COVID-19 NOTICIAS'}/>
            {
               news !== undefined ?
                  <div className={'body-news'}>
                     <div className={'news-content'}>

                        <div style={{ padding: '20px 20px 0 20px' }}>
                           <div className={'flex-space-between'}>
                              <h3 style={{ color: '#0378FF' }} className={'bold'}>{news.thread.section_title}</h3>
                              <span className={'bold'}>{moment(news.published).format('L')}</span>
                           </div>
                           <h2>{news.title}</h2>
                        </div>

                        <img alt={'News'} width={"100%"} height={450} style={{ objectFit: 'cover' }} src={news.thread.main_image}/>

                        <div style={{ padding: '20px 20px 0 20px' }}>
                           <LineSeparator size={'big'}/>
                           <p>Por <span className={'bold'}>{news.author !== '' ? news.author : news.thread.site}</span>
                           </p>
                           <LineSeparator size={'big'}/>
                           <p style={{ fontSize: 20 }}>{news.text}</p>
                        </div>

                     </div>
                     <div>

                     </div>
                  </div>
                  :
                  <div className={'no-results-container'}>
                     <h2>Noticia no disponible</h2>
                  </div>
            }
            <Footer/>
         </div>
   )
}

News.propTypes = {
   style: PropTypes.any
}

export default News
