import React, { useState } from 'react'
import PropTypes from 'prop-types'
import SignupForm from '../components/forms/SignupForm';
import { useHistory } from 'react-router-dom';
import { message } from 'antd';

const Signup = (props) => {

   const { style } = props

   const [users] = useState(JSON.parse(localStorage.getItem('usersNews')) || [])

   let history = useHistory();

   const signup = values => {

      const { email, password } = values

      const exist = users.find(x => x.email === email)

      if (exist) {
         message.warning('Lo sentimos, el usuario ya existe', 5);

      } else {
         users.push({
            email,
            password
         })

         localStorage.setItem('usersNews', JSON.stringify(users))
         localStorage.setItem('logged', JSON.stringify(true))

         history.push("/home");
      }
   }

   return (
      <div style={style} className={'signup-container'}>
         <h1 style={{ marginBottom: 20 }}>NOTICIAS COVID-19</h1>

         {/*Componente formulario de registro*/}
         <SignupForm
            submit={signup}
         />
      </div>
   )
}

Signup.propTypes = {
   style: PropTypes.any
}

export default Signup
