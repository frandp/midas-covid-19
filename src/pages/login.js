import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import LoginForm from '../components/forms/LoginForm'
import { message } from 'antd'
import { useHistory } from 'react-router-dom'

const Login = (props) => {

   const { style } = props

   const [users] = useState(JSON.parse(localStorage.getItem('usersNews')) || [])
   const [rememberUserNews] = useState(JSON.parse(localStorage.getItem('rememberUserNews')) || {})

   const logged = JSON.parse(localStorage.getItem('logged')) || false

   let history = useHistory()

   useEffect(() => {
      logged && history.push("/home")
   }, [history, logged])

   const login = values => {
      const { username, password, remember } = values

      const exist = users.find(x => (x.email === username && x.password === password))

      if (exist) {
         if (remember)
            localStorage.setItem('rememberUserNews', JSON.stringify({ username, password }))
         else
            localStorage.removeItem('rememberUserNews')

         localStorage.setItem('logged', JSON.stringify(true))

         history.push("/home")
      } else {
         message.error('Usuario o contraseña incorrecta!', 5)
      }
   }

   return (
      <div style={style} className={"login-container"}>
         <h1 style={{ marginBottom: 40 }}>NOTICIAS COVID-19</h1>
         {/*Componente formulario de logueo*/}
         <LoginForm
            submit={login}
            initialValues={{
               username: rememberUserNews.username || '',
               password: rememberUserNews.password || ''
            }}
         />
      </div>
   )
}

Login.propTypes = {
   style: PropTypes.any
}

export default Login
