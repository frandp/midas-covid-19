import axios from 'axios';

const baseUrl = "http://webhose.io/filterWebContent?token=b0c0b6ab-f0a1-46f7-98ee-8cc39c54e644&format=json&sort=crawled&q=coronavirus%20casos%20positivos%20%20language%3Aspanish%20thread.country%3AAR";

export const apiCall = (url, data, headers, method) => axios({
    method,
    url: baseUrl + url,
    data,
    headers
});
