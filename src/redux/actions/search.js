import { SEARCH_NEWS_START, SEARCH_NEWS_BY_ID_START } from '../../consts/actionTypes';

export const searchNews = payload => ({
   type: SEARCH_NEWS_START,
   payload
});

export const searchNewsById = payload => ({
   type: SEARCH_NEWS_BY_ID_START,
   payload
});
