import {
  SEARCH_NEWS_START,
  SEARCH_NEWS_ERROR,
  SEARCH_NEWS_COMPLETE,
  SEARCH_NEWS_BY_ID_START,
  SEARCH_NEWS_BY_ID_ERROR,
  SEARCH_NEWS_BY_ID_COMPLETE
} from '../../consts/actionTypes';
import { get } from 'lodash';

const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case SEARCH_NEWS_START:
      return { ...state, isLoading: true, news: null };
      break;
    case SEARCH_NEWS_COMPLETE:
      return { ...state, isLoading: false, newsResults: get(action, 'results.data') };
      break;
    case SEARCH_NEWS_ERROR:
      return { ...state, isLoading: false, news: null };
      break;
    case SEARCH_NEWS_BY_ID_START:
      return { ...state, isLoading: true, newsResult: null };
      break;
    case SEARCH_NEWS_BY_ID_COMPLETE:
      return { ...state, isLoading: false, newsResult: get(action, 'results.data') };
      break;
    case SEARCH_NEWS_BY_ID_ERROR:
      return { ...state, isLoading: false, newsResult: null };
      break;
    default:
      return { ...state };
  }
}
