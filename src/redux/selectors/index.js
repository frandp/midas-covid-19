import { get } from 'lodash';

export const isSearchingLoading = state => get(state, 'search.isLoading');
export const newsResults = state => get(state, 'search.newsResults.Search');
export const newsResult = state => get(state, 'search.newsResult');
