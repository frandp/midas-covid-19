import moment from 'moment';

export function filter_news(data, orderBy, categories) {

   let orderedData = []
   let filteredData = []

   switch (orderBy) {
      case 'date' : //Ordenamos por fecha
         orderedData = [...data].sort(function (a, b) {
            return moment.utc(b.published).diff(moment.utc(a.published))
         })
         break
      case 'relevance' : //Ordenamos por relevancia
         orderedData = [...data].sort(function (a, b) {
            return (b.thread.domain_rank - a.thread.domain_rank)
         })
         break
      default:
         orderedData = data
         break
   }

   //En el caso de existir categorias filtramos por las mismas
   if (categories !== undefined && categories.length > 0) {
      orderedData.forEach(x => {
         categories.forEach(w => {
            if (x.thread.site_categories.includes(w) && !filteredData.find(y => y === x))
               filteredData.push(x)
         })
      })
   } else {
      filteredData = orderedData
   }

   return filteredData
}

