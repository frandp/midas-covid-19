import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from './pages/login';
import Signup from './pages/signup';
import Home from './pages/home';
import News from './pages/news';

function App() {
   return (
      <Router>
         <div className={"container"}>
            <Route path="/" exact component={Login}/>
            <Route path="/signup" component={Signup}/>
            <Route path="/home" component={Home}/>
            <Route path="/news/:id" component={News}/>
         </div>
      </Router>
   )
}

export default App;
